const cloudinary = require("cloudinary");
const router = require("express").Router();
require("dotenv").config();

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_API_KEY,
  api_secret: process.env.CLOUD_API_SECRET,
});

router.delete("/:public_id", async (request, response) => {
  const { public_id } = req.params;
  try {
    // REMOVING THE IMAGE
    await cloudinary.uploader.destroy(public_id);
    response.status(200).send();
  } catch (error) {
    response.status(400).send(error.message);
  }
});

module.exports = router;
